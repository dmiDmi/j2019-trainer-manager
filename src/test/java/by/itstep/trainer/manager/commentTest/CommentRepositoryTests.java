package by.itstep.trainer.manager.commentTest;

import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.CommentRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentRepositoryTests {

    private CommentRepository commentRepository = new CommentRepositoryImpl();


    @BeforeEach
    void setUp() {
        commentRepository.deleteAll();
    }

    @Test
    void testFindAll() {

        // given

        CommentEntity comment1 = CommentEntity.builder()
                .firstName("A")
                .message("nice")
                .grade(8)
                .build();


        CommentEntity comment2 = CommentEntity.builder()
                .firstName("B")
                .message("good")
                .grade(5)
                .build();


        CommentEntity savedOne = commentRepository.create(comment1);
        CommentEntity savedTwo = commentRepository.create(comment2);

        // when

        List<CommentEntity> foundComments = commentRepository.findAll();

        // then

        Assertions.assertEquals(2, foundComments.size());
    }

    @Test
    void findById() {

        // given

        CommentEntity comment = CommentEntity.builder()
                .firstName("A")
                .message("nice")
                .grade(8)
                .build();

        CommentEntity saved = commentRepository.create(comment);

        // when

        CommentEntity foundComment = commentRepository.findById(saved.getId());

        // then
        Assertions.assertEquals(saved, foundComment);
        Assertions.assertNotNull(foundComment.getId());
    }

    @Test
    void testCreate() {

        // given
        CommentEntity comment = CommentEntity.builder()
                .firstName("A")
                .message("nice")
                .grade(8)
                .build();

        // when
        CommentEntity saved = commentRepository.create(comment);

        // then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate() {

        // given
        CommentEntity comment1 = CommentEntity.builder()
                .firstName("A")
                .message("nice")
                .grade(8)
                .build();

        CommentEntity saved = commentRepository.create(comment1);

        CommentEntity comment2 = CommentEntity.builder()
                .firstName("B")
                .message("good")
                .grade(5)
                .build();

        // when
        CommentEntity updateComment = commentRepository.update(comment2);

        // then
        Assertions.assertNotEquals(saved, comment2);

    }

    @Test
    void testDeleteById(){

        // given
        CommentEntity comment = CommentEntity.builder()
                .firstName("A")
                .message("nice")
                .grade(8)
                .build();

        CommentEntity saved = commentRepository.create(comment);

        // when
        commentRepository.deleteById(saved.getId());

        CommentEntity found = commentRepository.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }
}
