package by.itstep.trainer.manager.commentTest;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.CommentRepositoryImpl;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.service.CommentService;
import by.itstep.trainer.manager.service.CommentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentServiceTest {

    private CommentService service = new CommentServiceImpl();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @BeforeEach
    void setUp() {service.deleteAll(); }

    @Test
    void testFindAll() throws InvalidDtoException {
        // given
        for (int i = 0; i < 5; i++) {
            service.create(testCreateDto());
        }
        // when
        List<CommentPreviewDto> found = service.findAll();

        // then
        Assertions.assertEquals(5, found.size());

    }

    @Test
    void testFindById() throws InvalidDtoException {
        // given
        CommentFullDto saved = service.create(testCreateDto());

        // when
        CommentFullDto found = service.findById(saved.getId());

        // then
        Assertions.assertNotNull(found.getId());
        Assertions.assertEquals(saved, found);
    }

    @Test
    void testCreate() throws InvalidDtoException {
        // when
        CommentFullDto saved = service.create(testCreateDto());

        // then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate() throws InvalidDtoException {
        // given
        CommentFullDto comment1 = service.create(testCreateDto());

        CommentUpdateDto comment2 = new CommentUpdateDto();
        comment2.setId(comment1.getId());
        comment2.setMessage("bad");
        comment2.setGrade(2);
        comment2.setPublished("da");

        // when
        CommentFullDto updated = service.update(comment2);

        // then
        Assertions.assertNotEquals(comment1.getMessage(), updated.getMessage());
    }

    @Test
    void testDeleteById() throws InvalidDtoException {
        // given
        CommentFullDto saved = service.create(testCreateDto());

        // when
        service.deleteById(saved.getId());
        CommentFullDto found = service.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }




    private CommentCreateDto testCreateDto() {

        TrainerEntity trainer = TrainerEntity.builder()
                .firstName("Bob")
                .lastName("Bob")
                .workExperience("10")
                .email("bob@gmail.com")
                .build();

        Long trainerId = trainerRepository
                .create(trainer)
                .getId();

        CommentCreateDto commentCreateDto = new CommentCreateDto();
        commentCreateDto.setTrainerId(trainerId);
        commentCreateDto.setFirstName("July");
        commentCreateDto.setLastName("July");
        commentCreateDto.setMessage("Good");
        commentCreateDto.setGrade(5);

        return commentCreateDto;
    }

}
