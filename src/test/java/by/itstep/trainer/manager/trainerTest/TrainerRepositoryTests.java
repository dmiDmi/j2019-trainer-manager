package by.itstep.trainer.manager.trainerTest;


import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.repository.*;
import org.hibernate.Hibernate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class TrainerRepositoryTests {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();

    @BeforeEach
    void setUp() {
        commentRepository.deleteAll();
        appointmentRepository.deleteAll();
        trainerRepository.deleteAll();
    }

    @Test
    void testFindAll() {

        // given

        TrainerEntity trainer1 = TrainerEntity.builder()
                .firstName("Anna")
                .lastName("Anna")
                .workExperience("9")
                .build();



        TrainerEntity trainer2 = TrainerEntity.builder()
                .firstName("Kim")
                .lastName("Kim")
                .workExperience("2")
                .build();


        TrainerEntity savedOne = trainerRepository.create(trainer1);
        TrainerEntity savedTwo = trainerRepository.create(trainer2);



        // when

        List<TrainerEntity> foundTrainers = trainerRepository.findAll();

        // then

        Assertions.assertEquals(2, foundTrainers.size());
    }

    @Test
    void findById() {

        // given

        TrainerEntity trainer = TrainerEntity.builder()
                .firstName("Anna")
                .lastName("Anna")
                .workExperience("9")
                .build();

        TrainerEntity saved = trainerRepository.create(trainer);


        // when

        TrainerEntity foundTrainer = trainerRepository.findById(saved.getId());

        // then
        Assertions.assertEquals(saved, foundTrainer);
        Assertions.assertNotNull(foundTrainer.getId());
    }

    @Test
    void testCreate() {

        // given
        TrainerEntity trainer = TrainerEntity.builder()
                .firstName("Anna")
                .lastName("Anna")
                .workExperience("9")
                .build();

        // when
        TrainerEntity saved = trainerRepository.create(trainer);

        // then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate() {

        // given
        TrainerEntity trainer1 = TrainerEntity.builder()
                .firstName("Anna")
                .lastName("Anna")
                .workExperience("9")
                .build();

        TrainerEntity saved = trainerRepository.create(trainer1);

        TrainerEntity trainer2 = TrainerEntity.builder()
                .firstName("Nik")
                .lastName("Nik")
                .workExperience("7")
                .build();

        // when
        TrainerEntity updateTrainer = trainerRepository.update(trainer2);

        // then
        Assertions.assertNotEquals(saved, trainer2);

    }

    @Test
    void testDeleteById(){

        // given
        TrainerEntity trainer = TrainerEntity.builder()
                .firstName("Anna")
                .lastName("Anna")
                .workExperience("9")
                .build();

        TrainerEntity saved = trainerRepository.create(trainer);

        // when
        trainerRepository.deleteById(saved.getId());

        TrainerEntity found = trainerRepository.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }

    // при поиске по id ленивые подтягиваются

    @Test
    void testFindByIdWithTrainerAndAppoint() {

        // given
        TrainerEntity trainer = TrainerEntity.builder()
                .firstName("Mike")
                .lastName("Mike")
                .workExperience("7")
                .email("7788@gmail.com")
                .build();

        TrainerEntity saved = trainerRepository.create(trainer);


        CommentEntity comment1 = CommentEntity.builder()
                .firstName("lion")
                .message("345345")
                .trainer(saved)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .firstName("tiger")
                .message("789789")
                .trainer(saved)
                .build();

        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .firstNameVisitor("Julay")
                .time("18.00")
                .phoneNumber("123-465-78")
                .trainer(saved)// указываем тренера которого создали выше
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .firstNameVisitor("Viktor")
                .time("15.00")
                .phoneNumber("345-678-00")
                .trainer(saved)
                .build();

        commentRepository.create(comment1);
        commentRepository.create(comment2);

        appointmentRepository.create(appointment1);
        appointmentRepository.create(appointment2);

        // when

        TrainerEntity foundTrainer = trainerRepository.findById(saved.getId());

        // then
        Assertions.assertNotNull(foundTrainer);
        Assertions.assertNotNull(foundTrainer.getId());
        Assertions.assertNotNull(foundTrainer.getComments());
        Assertions.assertNotNull(foundTrainer.getAppointments());
        Assertions.assertEquals(2, foundTrainer.getComments().size());
        Assertions.assertEquals(2, foundTrainer.getAppointments().size());

    }

    // при поиске всех сразу ленивые не подтягиваются
    @Test
    void testFindAllWithoutTrainerAndAppoint() {

        // given
        TrainerEntity trainer = TrainerEntity.builder()
                .firstName("Mike")
                .lastName("Mike")
                .workExperience("7")
                .email("7788@gmail.com")
                .build();

        TrainerEntity saved = trainerRepository.create(trainer);


        CommentEntity comment1 = CommentEntity.builder()
                .firstName("lion")
                .message("345345")
                .trainer(saved)
                .build();

        CommentEntity comment2 = CommentEntity.builder()
                .firstName("tiger")
                .message("789789")
                .trainer(saved)
                .build();

        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .firstNameVisitor("July")
                .time("18.00")
                .phoneNumber("123-465-78")
                .trainer(saved)// указываем тренера которого создали выше
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .firstNameVisitor("Viktor")
                .time("15.00")
                .phoneNumber("345-678-00")
                .trainer(saved)
                .build();

        commentRepository.create(comment1);
        commentRepository.create(comment2);

        appointmentRepository.create(appointment1);
        appointmentRepository.create(appointment2);

        // when
        List<TrainerEntity> entityList = trainerRepository.findAll();
        for (TrainerEntity trainers : entityList) {

        // then
            Assertions.assertFalse(Hibernate.isInitialized(trainers.getComments()));
            Assertions.assertFalse(Hibernate.isInitialized(trainers.getAppointments()));
        }



    }

}
