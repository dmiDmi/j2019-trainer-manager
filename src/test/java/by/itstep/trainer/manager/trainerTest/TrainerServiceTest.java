package by.itstep.trainer.manager.trainerTest;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.service.CommentService;
import by.itstep.trainer.manager.service.CommentServiceImpl;
import by.itstep.trainer.manager.service.TrainerService;
import by.itstep.trainer.manager.service.TrainerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TrainerServiceTest {

    private TrainerService trainerService = new TrainerServiceImpl();
    private CommentService commentService = new CommentServiceImpl();

    @BeforeEach
    void setUp() {
        trainerService.deleteAll();
        commentService.deleteAll();
    }

    @Test
    void testFindAll() throws InvalidDtoException {
        // given
        for (int i = 0; i < 5; i++) {
            trainerService.create(testCreateDto());
        }
        // when
        List<TrainerPreviewDto> found = trainerService.findAll();

        // then
        Assertions.assertEquals(5, found.size());
    }

    @Test
    void testFindById() throws InvalidDtoException {
        // given
        TrainerFullDto saved = trainerService.create(testCreateDto());

        // when
        TrainerFullDto found = trainerService.findById(saved.getId());

        // then
        Assertions.assertNotNull(found.getId());
       // Assertions.assertEquals(saved, found);
    }

    @Test
    void testCreate() throws InvalidDtoException {
        // when
        TrainerFullDto saved = trainerService.create(testCreateDto());

        // then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate() throws InvalidDtoException {
        // given
        TrainerFullDto trainer1 = trainerService.create(testCreateDto());

        TrainerUpdateDto trainer2 = new TrainerUpdateDto();
        trainer2.setId(trainer1.getId());
        trainer2.setWorkExperience("5 years");
        trainer2.setEmail("dmi@gmail.com");

        // when
        TrainerFullDto updated = trainerService.update(trainer2);

        // then
        Assertions.assertNotEquals(trainer1.getWorkExperience(), updated.getWorkExperience());
    }

    @Test
    void testDeleteById() throws InvalidDtoException {
        // given
        TrainerFullDto saved = trainerService.create(testCreateDto());

        // when
        trainerService.deleteById(saved.getId());
        TrainerFullDto found = trainerService.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }

    private TrainerCreateDto testCreateDto() {

        TrainerCreateDto dto = new TrainerCreateDto();

        dto.setFirstName("Kai");
        dto.setLastName("Kai");
        dto.setWorkExperience("8 years");
        dto.setEmail("kai@gmail.com");
        dto.setPassword("!5675");

        return dto;
    }
}
