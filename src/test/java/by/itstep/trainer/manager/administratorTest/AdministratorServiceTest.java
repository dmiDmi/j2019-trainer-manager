package by.itstep.trainer.manager.administratorTest;

import by.itstep.trainer.manager.dto.administrator.AdministratorCreateDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorFullDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorUpdateDto;
import by.itstep.trainer.manager.entity.enums.Role;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;
import by.itstep.trainer.manager.service.AdministratorService;
import by.itstep.trainer.manager.service.AdministratorServiceImpl;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdministratorServiceTest {

    private AdministratorService service = new AdministratorServiceImpl();

    @BeforeEach
    void setUp() {
        service.deleteAll();
    }

    @Test
    void testFindAll() throws InvalidDtoException {

        // given

        for (int i = 0; i < 5; i++) {
            service.create(testAdminCreateDto());
        }
        // when
        List<AdministratorPreviewDto> found = service.findAll();

        // then
        Assertions.assertEquals(5, found.size());


    }
    @Test
    void testFindById() throws InvalidDtoException {

        // given
        AdministratorFullDto saved = service.create(testAdminCreateDto());

        // when
        AdministratorFullDto found = service.findById(saved.getId());

        // then
        Assertions.assertNotNull(found.getId());
        Assertions.assertEquals(saved, found);
    }

    @Test
    void testCreate() throws InvalidDtoException {
        // when
        AdministratorFullDto saved = service.create(testAdminCreateDto());

        // then
        Assertions.assertNotNull(saved.getId());

    }
    @Test
    void testUpdate() throws InvalidDtoException  {
        // given
        AdministratorFullDto admin1 = service.create(testAdminCreateDto());

        AdministratorUpdateDto admin2 = new AdministratorUpdateDto();
        admin2.setId(admin1.getId());
        admin2.setFirstName("Alex");
        admin2.setLastName("Alex");
        admin2.setEmail("alex@gmail.com");


        // when
        AdministratorFullDto update = service.update(admin2);

        // then
        Assertions.assertNotEquals(admin1, update);
    }
    @Test
    void testDeleteById() throws InvalidDtoException {
        // given
        AdministratorFullDto saved = service.create(testAdminCreateDto());

        // when
        service.deleteById(saved.getId());

        AdministratorFullDto found = service.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }


    private AdministratorCreateDto testAdminCreateDto()  {

        AdministratorCreateDto dto = new AdministratorCreateDto();
        dto.setFirstName("4444");
        dto.setLastName("5555");
        dto.setEmail("axe@gmail.com");
        dto.setRole(Role.SUPERUSER);

    return dto;
    }

}
