package by.itstep.trainer.manager.administratorTest;

import by.itstep.trainer.manager.entity.AdministratorEntity;
import by.itstep.trainer.manager.entity.enums.Role;
import by.itstep.trainer.manager.repository.AdministratorRepository;
import by.itstep.trainer.manager.repository.AdministratorRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class AdministratorRepositoryTests {

	private AdministratorRepository repository = new AdministratorRepositoryImpl();

	@BeforeEach
	void setUp() {
		repository.deleteAll();
	}


	@Test
	void testFindAll() {

		// GIVEN -подготавливаем базу данных к тесту
		AdministratorEntity administrator1 = AdministratorEntity.builder()
				.firstName("Bobi")
				.lastName("Axe")
				.email("bobi@gmail.com")
				.role(Role.ADMIN)
				.build();

		AdministratorEntity administrator2 = AdministratorEntity.builder()
				.firstName("Bobi5")
				.lastName("Axe5")
				.email("bobi5@gmail.com")
				.role(Role.ADMIN)
				.build();

		AdministratorEntity saved1 = repository.create(administrator1);
		AdministratorEntity saved2 = repository.create(administrator2);

		// WHEN
		List<AdministratorEntity> found = repository.findAll();

		// THEN
		Assertions.assertEquals(2, found.size());
	}

	@Test
	void testFindById() {

		// GIVEN
		AdministratorEntity administrator = AdministratorEntity.builder()
				.firstName("Alex")
				.lastName("Alex")
				.email("alex@gmail.com")
				.role(Role.ADMIN)
				.build();

		AdministratorEntity saved = repository.create(administrator);
		//Long id = saved.getId();
		// WHEN
		AdministratorEntity foundAdmin = repository.findById(saved.getId());

		// THEN
		Assertions.assertNotNull(foundAdmin.getId());
		Assertions.assertEquals(saved,foundAdmin);
	//	Assertions.assertTrue(saved.equals(foundAdmin));


	}

	@Test
    void testCreate() {

	    // given
        AdministratorEntity administrator = AdministratorEntity.builder()
                .firstName("Alex")
                .lastName("Alex")
                .email("alex@gmail.com")
                .role(Role.ADMIN)
                .build();


        // when
        AdministratorEntity saved = repository.create(administrator);

        // then
        Assertions.assertNotNull(saved.getId());

    }
    @Test
	void testUpdate() {

		// given
		AdministratorEntity administrator1 = AdministratorEntity.builder()
				.firstName("Alex")
				.lastName("Alex")
				.email("alex@gmail.com")
				.role(Role.ADMIN)
				.build();

		AdministratorEntity saved = repository.create(administrator1);

		AdministratorEntity administrator2 = AdministratorEntity.builder()
				.id(saved.getId())
				.firstName("Bobi")
				.lastName("Bobi")
				.email("bobi@gmail.com")
				.role(Role.SUPERUSER)
				.build();


		// when

		AdministratorEntity updateAdmin = repository.update(administrator2);

		// then
		Assertions.assertNotEquals(saved, updateAdmin);

	}

	@Test
	void testDeleteById() {

		// given
		AdministratorEntity administrator = AdministratorEntity.builder()
				.firstName("Alex")
				.lastName("Alex")
				.email("alex@gmail.com")
				.role(Role.ADMIN)
				.build();

		AdministratorEntity saved = repository.create(administrator);

		// when
		repository.deleteById(saved.getId());

		AdministratorEntity foundDelete = repository.findById(saved.getId());

		// then
		Assertions.assertNull(foundDelete);

	}


}
