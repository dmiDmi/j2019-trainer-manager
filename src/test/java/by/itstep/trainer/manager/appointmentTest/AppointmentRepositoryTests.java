package by.itstep.trainer.manager.appointmentTest;

import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.repository.AppointmentRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class AppointmentRepositoryTests {

    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();


    @BeforeEach
    void setUp() {
        appointmentRepository.deleteAll();
    }

    @Test
    void testFindAll() {

        // given

        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .firstNameVisitor("Dan")
                .time("18.00")
                .phoneNumber("123-123-56")
                .build();

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .firstNameVisitor("Nik")
                .time("20.00")
                .phoneNumber("555-777-89")
                .build();

        AppointmentEntity savedOne = appointmentRepository.create(appointment1);
        AppointmentEntity savedTwo = appointmentRepository.create(appointment2);

        // when

        List<AppointmentEntity> foundAppointments = appointmentRepository.findAll();

        // then

        Assertions.assertEquals(2, foundAppointments.size());
    }

    @Test
    void findById() {

        // given

        AppointmentEntity appointment = AppointmentEntity.builder()
                .firstNameVisitor("Dan")
                .time("18.00")
                .phoneNumber("123-123-56")
                .build();

        AppointmentEntity saved = appointmentRepository.create(appointment);

        // when

        AppointmentEntity foundAppointment = appointmentRepository.findById(saved.getId());

        // then
        Assertions.assertEquals(saved, foundAppointment);
        Assertions.assertNotNull(foundAppointment.getId());
    }

    @Test
    void testCreate() {

        // given
        AppointmentEntity appointment = AppointmentEntity.builder()
                .firstNameVisitor("Dan")
                .time("18.00")
                .phoneNumber("123-123-56")
                .build();

        // when
        AppointmentEntity saved = appointmentRepository.create(appointment);

        // then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate() {

        // given
        AppointmentEntity appointment1 = AppointmentEntity.builder()
                .firstNameVisitor("Dan")
                .time("18.00")
                .phoneNumber("123-123-56")
                .build();

        AppointmentEntity saved = appointmentRepository.create(appointment1);

        AppointmentEntity appointment2 = AppointmentEntity.builder()
                .firstNameVisitor("Nik")
                .time("20.00")
                .phoneNumber("555-777-89")
                .build();

        // when
        AppointmentEntity updateAppointment = appointmentRepository.update(appointment2);

        // then
        Assertions.assertNotEquals(saved, appointment2);

    }

    @Test
    void testDeleteById(){

        // given
        AppointmentEntity appointment = AppointmentEntity.builder()
                .firstNameVisitor("Dan")
                .time("18.00")
                .phoneNumber("123-123-56")
                .build();

        AppointmentEntity saved = appointmentRepository.create(appointment);

        // when
        appointmentRepository.deleteById(saved.getId());

        AppointmentEntity found = appointmentRepository.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }
}
