package by.itstep.trainer.manager.appointmentTest;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.service.AppointmentService;
import by.itstep.trainer.manager.service.AppointmentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AppointmentServiceTest {

    private AppointmentService service = new AppointmentServiceImpl();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @BeforeEach
    void setUp() {
        service.deleteAll();
    }

    @Test
    void testFindAll() throws InvalidDtoException {
        // given
        for (int i = 0; i < 5; i++) {
            service.create(testCreateDto());
        }
        // when
        List<AppointmentPreviewDto> found = service.findAll();

        // then
        Assertions.assertEquals(5, found.size());
    }

    @Test
    void testFindById() throws InvalidDtoException {

        // given
        AppointmentFullDto saved = service.create(testCreateDto());

        // when
        AppointmentFullDto found = service.findById(saved.getId());

        // then
        Assertions.assertNotNull(found.getId());
        Assertions.assertEquals(saved, found);
    }

    @Test
    void testCreate() throws InvalidDtoException {
        // when
        AppointmentFullDto saved = service.create(testCreateDto());

        // then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate() throws InvalidDtoException {
        // given
        AppointmentFullDto appointment1 = service.create(testCreateDto());

        AppointmentUpdateDto appointment2 = new AppointmentUpdateDto();
        appointment2.setId(appointment1.getId());
        appointment2.setTime("21.00");

        // when
        AppointmentFullDto updated = service.update(appointment2);

        // then
        Assertions.assertNotEquals(appointment1.getTime(), updated.getTime());

    }

    @Test
    void testDeleteById() throws InvalidDtoException {
        // given
        AppointmentFullDto saved = service.create(testCreateDto());

        // when
        service.deleteById(saved.getId());
        AppointmentFullDto found = service.findById(saved.getId());

        // then
        Assertions.assertNull(found);
    }

   private AppointmentCreateDto testCreateDto() {

       TrainerEntity trainer = TrainerEntity.builder()
               .firstName("Alex")
               .lastName("Alex")
               .workExperience("20")
               .email("alex@gmail.com")
               .build();

       Long trainerId = trainerRepository
               .create(trainer)
               .getId();

       AppointmentCreateDto appointmentCreateDto = new AppointmentCreateDto();
       appointmentCreateDto.setFirstNameVisitor("Boris");
       appointmentCreateDto.setLastNameVisitor("Boris");
       appointmentCreateDto.setTime("20.00");
       appointmentCreateDto.setPhoneNumber("2245-5656");
       appointmentCreateDto.setTrainerId(trainerId);

       return appointmentCreateDto;
   }
}
