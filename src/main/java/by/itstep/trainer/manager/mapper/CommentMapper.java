package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<CommentEntity> entities) {

        List<CommentPreviewDto> dtoList = new ArrayList<>();

        for (CommentEntity entity : entities) {

            CommentPreviewDto previewDto = new CommentPreviewDto();
            previewDto.setId(entity.getId());
            previewDto.setTrainerId(entity.getTrainer().getId());
            previewDto.setFirstName(entity.getFirstName());
            previewDto.setLastName(entity.getLastName());

            dtoList.add(previewDto)  ;
        }
            return dtoList;
    }

    public CommentFullDto mapToDto(CommentEntity entity) {

        CommentFullDto fullDto = new CommentFullDto();
        fullDto.setId(entity.getId());
        fullDto.setTrainerId(entity.getTrainer().getId());
        fullDto.setMessage(entity.getMessage());
        fullDto.setFirstName(entity.getFirstName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setEmail(entity.getEmail());
        fullDto.setGrade(entity.getGrade());
        fullDto.setPublished(entity.getPublished());

        return fullDto;
    }

    public CommentEntity mapToEntity(CommentCreateDto createDto, TrainerEntity trainer) {

        CommentEntity entity = new CommentEntity();

        entity.setMessage(createDto.getMessage());
        entity.setFirstName(createDto.getFirstName());
        entity.setLastName(createDto.getLastName());
        entity.setGrade(createDto.getGrade());
        entity.setTrainer(trainer);

        return entity;
    }

    public CommentEntity mapToEntity(CommentUpdateDto updateDto) {

        CommentEntity entity = new CommentEntity();
        entity.setId(updateDto.getId());
        entity.setMessage(updateDto.getMessage());
        entity.setGrade(updateDto.getGrade());
        entity.setPublished(updateDto.getPublished());

        return entity;
    }
}
