package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.administrator.AdministratorCreateDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorFullDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorUpdateDto;
import by.itstep.trainer.manager.entity.AdministratorEntity;

import java.util.ArrayList;
import java.util.List;

public class AdministratorMapper {

    public List<AdministratorPreviewDto> mapToDtoList(List<AdministratorEntity> entities) {

        List<AdministratorPreviewDto> dtoList = new ArrayList<>();

        for (AdministratorEntity entity : entities) {

            AdministratorPreviewDto dto = new AdministratorPreviewDto();
            dto.setId(entity.getId());
            dto.setFirstName(entity.getFirstName());
            dto.setLastName(entity.getLastName());
            dto.setEmail(entity.getEmail());

            dtoList.add(dto);
        }

        return dtoList;
    }

    public AdministratorFullDto mapToDto(AdministratorEntity entity) {

        AdministratorFullDto fullDto = new AdministratorFullDto();
        fullDto.setId(entity.getId());
        fullDto.setFirstName(entity.getFirstName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setEmail(entity.getEmail());
        fullDto.setRole(entity.getRole());

        return fullDto;
    }

    public AdministratorEntity mapToEntity(AdministratorCreateDto createDto) {

        AdministratorEntity administratorEntity = new AdministratorEntity();
        administratorEntity.setFirstName(createDto.getFirstName());
        administratorEntity.setLastName(createDto.getLastName());
        administratorEntity.setEmail(createDto.getEmail());
        administratorEntity.setRole(createDto.getRole());

        return administratorEntity;

    }

    public AdministratorEntity mapToEntity(AdministratorUpdateDto updateDto) {

        AdministratorEntity entity = new AdministratorEntity();
        entity.setId(updateDto.getId());
        entity.setFirstName(updateDto.getFirstName());
        entity.setLastName(updateDto.getLastName());
        entity.setEmail(updateDto.getEmail());

        return entity;
    }


}
