package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDoList(List<TrainerEntity> entities) {

        List<TrainerPreviewDto> dtoList = new ArrayList<>();

        for (TrainerEntity entity : entities) {

            TrainerPreviewDto previewDto = new TrainerPreviewDto();
            previewDto.setId(entity.getId());
            previewDto.setFirstName(entity.getFirstName());
            previewDto.setLastName(entity.getLastName());
            previewDto.setWorkExperience(entity.getWorkExperience());
            previewDto.setAvatar(entity.getAvatar());

            dtoList.add(previewDto);
        }
        return dtoList;
    }

    public TrainerFullDto mapToDo(TrainerEntity entity) {

        TrainerFullDto fullDto = new TrainerFullDto();
        fullDto.setId(entity.getId());
        fullDto.setFirstName(entity.getFirstName());
        fullDto.setLastName(entity.getLastName());
        fullDto.setWorkExperience(entity.getWorkExperience());
        fullDto.setComments(entity.getComments());
        fullDto.setAppointments(entity.getAppointments());
        fullDto.setAvatar(entity.getAvatar());
        fullDto.setEmail(entity.getEmail());
        fullDto.setPassword(entity.getPassword());

        return fullDto;
    }

    public TrainerEntity mapToEntity(TrainerCreateDto createDto) {

       TrainerEntity entity = new TrainerEntity() ;
       entity.setFirstName(createDto.getFirstName());
       entity.setLastName(createDto.getLastName());
       entity.setWorkExperience(createDto.getWorkExperience());
       entity.setAvatar(createDto.getAvatar());
       entity.setEmail(createDto.getEmail());
       entity.setPassword(createDto.getPassword());

       return entity;
    }

    public TrainerEntity mapToEntity(TrainerUpdateDto updateDto) {

        TrainerEntity entity = new TrainerEntity() ;
        entity.setId(updateDto.getId());
        entity.setWorkExperience(updateDto.getWorkExperience());
        entity.setEmail(updateDto.getEmail());

        return entity;
    }
}
