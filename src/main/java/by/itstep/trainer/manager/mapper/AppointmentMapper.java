package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;

import java.util.ArrayList;
import java.util.List;

public class AppointmentMapper {


    public List<AppointmentPreviewDto> mapToDtoList(List<AppointmentEntity> entities) {

        List<AppointmentPreviewDto> dtoList = new ArrayList<>();

        for (AppointmentEntity entity : entities) {

            AppointmentPreviewDto previewDto = new AppointmentPreviewDto();

            previewDto.setId(entity.getId());
            previewDto.setFirstNameVisitor(entity.getFirstNameVisitor());
            previewDto.setLastNameVisitor(entity.getLastNameVisitor());
            previewDto.setTime(entity.getTime());
            previewDto.setPhoneNumber(entity.getPhoneNumber());

            dtoList.add(previewDto);

        }
        return dtoList;
    }

    public AppointmentFullDto mapToDto(AppointmentEntity entity) {


        AppointmentFullDto fullDto = new AppointmentFullDto();

        fullDto.setId(entity.getId());
        fullDto.setTrainerId(entity.getTrainer().getFirstName() + " " + entity.getTrainer().getLastName());
        fullDto.setTime(entity.getTime());
        fullDto.setPostComment(entity.getPostComment());
        fullDto.setFirstNameVisitor(entity.getFirstNameVisitor());
        fullDto.setLastNameVisitor(entity.getLastNameVisitor());
        fullDto.setEmail(entity.getEmail());
        fullDto.setPhoneNumber(entity.getPhoneNumber());

        return fullDto;
    }

    public AppointmentEntity mapToEntity(AppointmentCreateDto createDto, TrainerEntity trainer) {


        AppointmentEntity appointmentEntity = new AppointmentEntity();

        appointmentEntity.setTime(createDto.getTime());
        appointmentEntity.setFirstNameVisitor(createDto.getFirstNameVisitor());
        appointmentEntity.setLastNameVisitor(createDto.getLastNameVisitor());
        appointmentEntity.setPhoneNumber(createDto.getPhoneNumber());
        appointmentEntity.setTrainer(trainer);

        return appointmentEntity;
    }

    public AppointmentEntity mapToEntity(AppointmentUpdateDto updateDto) {

        AppointmentEntity entity = new AppointmentEntity();
        entity.setId(updateDto.getId());
        entity.setTime(updateDto.getTime());

        return entity;
    }
}
