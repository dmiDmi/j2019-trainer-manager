package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.administrator.AdministratorCreateDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorFullDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorUpdateDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;

import java.util.List;

public interface AdministratorService {

    List<AdministratorPreviewDto> findAll();

    AdministratorFullDto findById(Long id);

    AdministratorFullDto create(AdministratorCreateDto createDto) throws InvalidDtoException;

    AdministratorFullDto update(AdministratorUpdateDto updateDto) throws MissedUpdateIdException;

    void deleteById(Long id);

    void deleteAll();
}
