package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;

import java.util.List;

public interface TrainerService {

    List<TrainerPreviewDto> findAll();

    TrainerFullDto findById(Long id);

    TrainerFullDto create(TrainerCreateDto createDto) throws InvalidDtoException;

    TrainerFullDto update(TrainerUpdateDto updateDto) throws MissedUpdateIdException;

    void deleteById(Long id);

    void deleteAll();
}
