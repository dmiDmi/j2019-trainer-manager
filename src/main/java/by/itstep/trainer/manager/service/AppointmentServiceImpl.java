package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;
import by.itstep.trainer.manager.mapper.AppointmentMapper;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.repository.AppointmentRepositoryImpl;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class AppointmentServiceImpl implements AppointmentService {

    private AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();
    private AppointmentMapper mapper = new AppointmentMapper();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();


    @Override
    public List<AppointmentPreviewDto> findAll() {

        List<AppointmentEntity> found = appointmentRepository.findAll();
        return mapper.mapToDtoList(found);

    }

    @Override
    public AppointmentFullDto findById(Long id) {

        AppointmentEntity foundAppointment = appointmentRepository.findById(id);
        return mapper.mapToDto(foundAppointment);

    }

    @Override
    public AppointmentFullDto create(AppointmentCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        TrainerEntity trainer = trainerRepository.findById(createDto.getTrainerId());

        AppointmentEntity toSave = mapper.mapToEntity(createDto, trainer);
        AppointmentEntity created = appointmentRepository.create(toSave);
        AppointmentFullDto response = mapper.mapToDto(created);

        return response;
    }

    @Override
    public AppointmentFullDto update(AppointmentUpdateDto updateDto) throws MissedUpdateIdException {

        validateUpdateDto(updateDto);

        AppointmentEntity entityToUpdate = mapper.mapToEntity(updateDto);

        AppointmentEntity existingEntity = appointmentRepository.findById(updateDto.getId());

        entityToUpdate.setPostComment(existingEntity.getPostComment());
        entityToUpdate.setFirstNameVisitor(existingEntity.getFirstNameVisitor());
        entityToUpdate.setLastNameVisitor(existingEntity.getLastNameVisitor());
        entityToUpdate.setEmail(existingEntity.getEmail());
        entityToUpdate.setPhoneNumber(existingEntity.getPhoneNumber());


        AppointmentEntity updated = appointmentRepository.update(entityToUpdate);

        AppointmentFullDto response = mapper.mapToDto(updated);
        return response;
    }

    @Override
    public void deleteById(Long id) {

        appointmentRepository.deleteById(id);

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM appointment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    private void validateCreateDto(AppointmentCreateDto dto) throws InvalidDtoException {

        if (dto.getTrainerId() == null || dto.getTime() == null || dto.getFirstNameVisitor() == null ||
            dto.getLastNameVisitor() == null || dto.getPhoneNumber() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }
        if (dto.getPhoneNumber().length() < 9) {
            throw new InvalidDtoException("Phone number is at least 9 digits");
        }

    }
    private void validateUpdateDto(AppointmentUpdateDto dto) throws MissedUpdateIdException {
        if (dto.getId() == null) {

            throw new MissedUpdateIdException("Id is not specified");
        }
    }
}
