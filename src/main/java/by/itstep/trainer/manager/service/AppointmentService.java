package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;

import java.util.List;

public interface AppointmentService {

    List<AppointmentPreviewDto> findAll();

    AppointmentFullDto findById(Long id);

    AppointmentFullDto create(AppointmentCreateDto createDto) throws InvalidDtoException;

    AppointmentFullDto update(AppointmentUpdateDto updateDto) throws MissedUpdateIdException;

    void deleteById(Long id);

    void deleteAll();
}
