package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;
import by.itstep.trainer.manager.mapper.CommentMapper;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.CommentRepositoryImpl;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository = new CommentRepositoryImpl();
    private CommentMapper mapper = new CommentMapper();
    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @Override
    public List<CommentPreviewDto> findAll() {
        List<CommentEntity> found = commentRepository.findAll();
        return mapper.mapToDtoList(found);
    }

    @Override
    public CommentFullDto findById(Long id) {

        CommentEntity foundComment = commentRepository.findById(id);

        return mapper.mapToDto(foundComment);
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        TrainerEntity trainer = trainerRepository.findById(createDto.getTrainerId());
        CommentEntity toSave = mapper.mapToEntity(createDto, trainer);
        CommentEntity created = commentRepository.create(toSave);
        CommentFullDto response = mapper.mapToDto(created);
        return response;
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) throws MissedUpdateIdException {

        validateUpdateDto(updateDto);

        CommentEntity entityToUpdate = mapper.mapToEntity(updateDto);
        CommentEntity existingEntity = commentRepository.findById(updateDto.getId());

        entityToUpdate.setTrainer(existingEntity.getTrainer());
        entityToUpdate.setFirstName(existingEntity.getFirstName());
        entityToUpdate.setLastName(existingEntity.getLastName());
        entityToUpdate.setEmail(existingEntity.getEmail());

        CommentEntity updated = commentRepository.update(entityToUpdate);
        CommentFullDto response = mapper.mapToDto(updated);
        return response;
    }

    @Override
    public void deleteById(Long id) {

        commentRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    private void validateCreateDto(CommentCreateDto dto) throws InvalidDtoException {
        if (dto.getTrainerId() == null || dto.getFirstName() == null || dto.getLastName() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }
        if (dto.getGrade() < 1 && dto.getGrade() > 10  ) {

            throw new InvalidDtoException("Wrong estimate");

        }
    }

    private void validateUpdateDto(CommentUpdateDto dto) throws MissedUpdateIdException {
        if (dto.getId() == null) {

            throw new MissedUpdateIdException("Id is not specified");
        }
    }
}
