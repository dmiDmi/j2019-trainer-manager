package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;
import by.itstep.trainer.manager.mapper.TrainerMapper;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.TrainerRepositoryImpl;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class TrainerServiceImpl implements TrainerService {

    private TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private TrainerMapper mapper = new TrainerMapper();

    @Override
    public List<TrainerPreviewDto> findAll() {

        List<TrainerEntity> foundTrainer = trainerRepository.findAll();
        return mapper.mapToDoList(foundTrainer);
    }

    @Override
    public TrainerFullDto findById(Long id) {

        TrainerEntity foundTrainer = trainerRepository.findById(id);
        return mapper.mapToDo(foundTrainer);

    }

    @Override
    public TrainerFullDto create(TrainerCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        TrainerEntity toSave = mapper.mapToEntity(createDto);
        TrainerEntity created = trainerRepository.create(toSave);
        TrainerFullDto response = mapper.mapToDo(created);

        return response;
    }

    @Override
    public TrainerFullDto update(TrainerUpdateDto updateDto) throws MissedUpdateIdException{

        validateUpdateDto(updateDto);

        TrainerEntity entityToUpdate = mapper.mapToEntity(updateDto);
        TrainerEntity existingEntity = trainerRepository.findById(updateDto.getId());

        entityToUpdate.setFirstName(existingEntity.getFirstName());
        entityToUpdate.setLastName(existingEntity.getLastName());
        entityToUpdate.setComments(existingEntity.getComments());
        entityToUpdate.setAppointments(existingEntity.getAppointments());
        entityToUpdate.setAvatar(existingEntity.getAvatar());
        entityToUpdate.setPassword(existingEntity.getPassword());

        TrainerEntity updated = trainerRepository.update(entityToUpdate);

        TrainerFullDto response = mapper.mapToDo(updated);

        return response;
    }

    @Override
    public void deleteById(Long id) {

        trainerRepository.deleteById(id);

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    private void validateCreateDto(TrainerCreateDto dto) throws InvalidDtoException {

        if (dto.getFirstName() == null || dto.getLastName() == null || dto.getWorkExperience() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }
        if (!dto.getPassword().contains("!") && !dto.getPassword().contains("#")) {

            throw new InvalidDtoException("Must contain the following characters");
        }
    }
    private void validateUpdateDto(TrainerUpdateDto dto) throws MissedUpdateIdException {

        if (dto.getId() == null) {
            throw new MissedUpdateIdException("Id is not specified");
        }
    }
}
