package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.administrator.AdministratorCreateDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorFullDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.administrator.AdministratorUpdateDto;
import by.itstep.trainer.manager.entity.AdministratorEntity;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;
import by.itstep.trainer.manager.mapper.AdministratorMapper;
import by.itstep.trainer.manager.repository.AdministratorRepository;
import by.itstep.trainer.manager.repository.AdministratorRepositoryImpl;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class AdministratorServiceImpl implements AdministratorService {

    private AdministratorRepository administratorRepository = new AdministratorRepositoryImpl();
    private AdministratorMapper mapper = new AdministratorMapper();

    @Override
    public List<AdministratorPreviewDto> findAll() {

        List<AdministratorEntity> foundAdmin = administratorRepository.findAll();
        return mapper.mapToDtoList(foundAdmin);
    }

    @Override
    public AdministratorFullDto findById(Long id) {

        AdministratorEntity foundAdmin = administratorRepository.findById(id);
        AdministratorFullDto response = mapper.mapToDto(foundAdmin);
        return response;

    }

    @Override
    public AdministratorFullDto create(AdministratorCreateDto createDto) throws InvalidDtoException {

        validateCreateDto(createDto);

        AdministratorEntity toSave = mapper.mapToEntity(createDto);// есть все данные из createDto
        AdministratorEntity created = administratorRepository.create(toSave);
        AdministratorFullDto response = mapper.mapToDto(created);

        return response;
    }

    @Override
    public AdministratorFullDto update(AdministratorUpdateDto updateDto) throws MissedUpdateIdException {

        validateUpdateDto(updateDto);

        AdministratorEntity entityToUpdate = mapper.mapToEntity(updateDto);// получаем entity
        // которую нужно обновить. у неё старый id и новые данные
        AdministratorEntity existingEntity = administratorRepository.findById(updateDto.getId());
        // берем оригинальную(существующую) entity

        entityToUpdate.setRole(existingEntity.getRole());// заполняем данными из существующей entity

        AdministratorEntity updated = administratorRepository.update(entityToUpdate);

        AdministratorFullDto response = mapper.mapToDto(updated);// превращаем в fullDto чтобы вернуть
        return response;
    }

    @Override
    public void deleteById(Long id) {

        administratorRepository.deleteById(id);

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM administrator").executeUpdate();

        em.getTransaction().commit();
        em.close();


    }

    private void validateCreateDto(AdministratorCreateDto dto) throws InvalidDtoException {

        if (dto.getFirstName() == null || dto.getLastName() == null ||
            dto.getEmail() == null || dto.getRole() == null) {

            throw new InvalidDtoException("One or more fields is null");
        }

        if (!dto.getEmail().contains("@")){

        throw new InvalidDtoException("Does not contain @ sign");
        }


    }

    private void validateUpdateDto(AdministratorUpdateDto dto) throws MissedUpdateIdException {
        if (dto.getId() == null) {

            throw new MissedUpdateIdException("Id is not specified");
        }
    }
}
