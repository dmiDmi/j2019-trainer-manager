package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.exception.MissedUpdateIdException;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto) throws InvalidDtoException;

    CommentFullDto update(CommentUpdateDto updateDto) throws MissedUpdateIdException;

    void deleteById(Long id);

    void deleteAll();
}
