package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.trainer.TrainerLoginDto;

public interface AuthService {

    void login(TrainerLoginDto loginDto);

    void logout(String email);

    boolean isAuthenticated(String email);

    Long getAuthenticatedId(String email);
}
