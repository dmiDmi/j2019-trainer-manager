package by.itstep.trainer.manager.exception;

public class InvalidDtoException extends Exception {

    public InvalidDtoException(String message) {
        super("Danger" + message);
    }

}
