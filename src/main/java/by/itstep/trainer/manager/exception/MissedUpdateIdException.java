package by.itstep.trainer.manager.exception;

public class MissedUpdateIdException extends InvalidDtoException {

    public MissedUpdateIdException(String message) {
        super("Danger" + message);
    }


}
