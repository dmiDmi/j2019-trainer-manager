package by.itstep.trainer.manager.entity.enums;

public enum Role {

    ADMIN,
    SUPERUSER
}
