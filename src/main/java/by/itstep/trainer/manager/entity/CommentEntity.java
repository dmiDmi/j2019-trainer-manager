package by.itstep.trainer.manager.entity;


import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class CommentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne  //много комментов, у каждого тренера
    @JoinColumn(name = "trainer_id")
    private TrainerEntity trainer;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "grade")
    private int grade; //оценка

    @Column(name = "published")
    private String published;
}
