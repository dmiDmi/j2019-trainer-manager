package by.itstep.trainer.manager.entity;


import lombok.*;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "appointment")
public class AppointmentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne  //много записей к одному тренеру
    @JoinColumn(name = "trainer_id")
    private TrainerEntity trainer; // к какому тренеру записались

    @Column(name = "time", nullable = false)
    private String time;

    @Column(name = "post_comment")
    private String postComment;

    @Column(name = "first_name_visitor", nullable = false)
    private String firstNameVisitor;

    @Column(name = "last_name_visitor")
    private String lastNameVisitor;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

}
