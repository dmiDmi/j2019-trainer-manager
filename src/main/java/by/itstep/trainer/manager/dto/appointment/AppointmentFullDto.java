package by.itstep.trainer.manager.dto.appointment;

import by.itstep.trainer.manager.entity.TrainerEntity;
import lombok.Data;

@Data
public class AppointmentFullDto {

    private Long id;

    private String trainerId;

    private String time;

    private String postComment;

    private String firstNameVisitor;

    private String lastNameVisitor;

    private String email;

    private String phoneNumber;


}
