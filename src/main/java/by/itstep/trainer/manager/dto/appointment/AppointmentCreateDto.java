package by.itstep.trainer.manager.dto.appointment;

import lombok.Data;

@Data
public class AppointmentCreateDto {

    private Long trainerId;

    private String time;

    private String firstNameVisitor;

    private String lastNameVisitor;

    private String phoneNumber;
}
