package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerPreviewDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String workExperience;

    private String avatar;
}
