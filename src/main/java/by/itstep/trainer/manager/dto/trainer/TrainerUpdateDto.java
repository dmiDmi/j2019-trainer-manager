package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerUpdateDto {

    private Long id;

    private String workExperience;

    private String email;
}
