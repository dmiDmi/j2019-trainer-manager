package by.itstep.trainer.manager.dto.trainer;

import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.entity.CommentEntity;
import lombok.Data;

import java.util.List;

@Data
public class TrainerFullDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String workExperience;

    private List<CommentEntity> comments;

    private List<AppointmentEntity> appointments;

    private String avatar;

    private String email;

    private String password;
}
