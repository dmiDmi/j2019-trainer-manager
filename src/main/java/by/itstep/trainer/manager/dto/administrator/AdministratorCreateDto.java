package by.itstep.trainer.manager.dto.administrator;

import by.itstep.trainer.manager.entity.enums.Role;
import lombok.Data;

@Data
public class AdministratorCreateDto {

    private String firstName;

    private String lastName;

    private String email;

    private Role role;
}
