package by.itstep.trainer.manager.dto.comment;

import lombok.Data;

@Data
public class CommentUpdateDto {

    private Long id;

    private String message;

    private int grade;

    private String published;
}
