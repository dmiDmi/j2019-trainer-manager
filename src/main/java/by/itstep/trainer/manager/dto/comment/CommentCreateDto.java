package by.itstep.trainer.manager.dto.comment;

import lombok.Data;

@Data
public class CommentCreateDto {

    private Long trainerId;

    private String message;

    private String firstName;

    private String lastName;

    private int grade;
}
