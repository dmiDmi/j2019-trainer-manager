package by.itstep.trainer.manager.dto.administrator;

import lombok.Data;

@Data
public class AdministratorUpdateDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;
}
