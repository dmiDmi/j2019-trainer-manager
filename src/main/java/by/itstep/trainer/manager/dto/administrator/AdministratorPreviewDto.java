package by.itstep.trainer.manager.dto.administrator;

import lombok.Builder;
import lombok.Data;

@Data

public class AdministratorPreviewDto { // отображаются в фронтенд

    private Long id;

    private String firstName;

    private String lastName;

    private String email;
}
