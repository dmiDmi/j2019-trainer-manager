package by.itstep.trainer.manager.dto.appointment;

import lombok.Data;

@Data
public class AppointmentPreviewDto {

    private Long id;

    private String time;

    private String firstNameVisitor;

    private String lastNameVisitor;

    private String phoneNumber;
}
