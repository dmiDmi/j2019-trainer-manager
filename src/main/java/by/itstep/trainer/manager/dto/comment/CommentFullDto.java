package by.itstep.trainer.manager.dto.comment;

import lombok.Data;

@Data
public class CommentFullDto {

    private Long id;

    private Long trainerId;

    private String message;

    private String firstName;

    private String lastName;

    private String email;

    private int grade;

    private String published;
}
