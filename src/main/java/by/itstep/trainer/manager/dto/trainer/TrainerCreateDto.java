package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerCreateDto {

    private String firstName;

    private String lastName;

    private String workExperience;

    private String avatar;

    private String email;

    private String password;
}
