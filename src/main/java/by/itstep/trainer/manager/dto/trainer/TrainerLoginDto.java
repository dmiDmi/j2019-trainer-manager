package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerLoginDto {

    private String email;
    private String password;
}
