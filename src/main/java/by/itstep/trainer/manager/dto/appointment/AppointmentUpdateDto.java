package by.itstep.trainer.manager.dto.appointment;

import lombok.Data;

@Data
public class AppointmentUpdateDto {

    private Long id;

    private String time;

}
