package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.CommentEntity;

import java.util.List;

public interface CommentRepository {

    List<CommentEntity> findAll();

    CommentEntity findById(Long id);

    CommentEntity create (CommentEntity commentEntity);

    CommentEntity update (CommentEntity commentEntity);

    void deleteById(Long id);

    void deleteAll();
}
