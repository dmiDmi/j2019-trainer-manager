package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.TrainerEntity;

import java.util.List;

public interface TrainerRepository {

    List<TrainerEntity> findAll();

    TrainerEntity findById(Long id);

    TrainerEntity create (TrainerEntity trainerEntity);

    TrainerEntity update (TrainerEntity trainerEntity);

    void deleteById(Long id);

    void deleteAll();
}
