package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.AppointmentEntity;

import java.util.List;

public interface AppointmentRepository {

    List<AppointmentEntity> findAll();

    AppointmentEntity findById(Long id);

    AppointmentEntity create (AppointmentEntity appointmentEntity);

    AppointmentEntity update (AppointmentEntity appointmentEntity);

    void deleteById(Long id);

    void deleteAll();
}
