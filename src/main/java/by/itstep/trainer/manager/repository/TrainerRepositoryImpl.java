package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.entity.TrainerEntity;
import by.itstep.trainer.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;

import javax.persistence.EntityManager;
import java.util.List;

public class TrainerRepositoryImpl implements TrainerRepository {
    @Override
    public List<TrainerEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<TrainerEntity> foundAllTrainer = em.createNativeQuery("SELECT * FROM trainer",
                TrainerEntity.class).getResultList();

        em.close();
        System.out.println("found" + foundAllTrainer.size() + " trainers");
        return  foundAllTrainer;
    }

    @Override
    public TrainerEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        TrainerEntity foundOneTrainer = em.find(TrainerEntity.class, id);

        if(foundOneTrainer != null) {
            Hibernate.initialize(foundOneTrainer.getComments());
            Hibernate.initialize(foundOneTrainer.getAppointments());
        }
        em.close();
        System.out.println("found trainer: " + foundOneTrainer);
        return foundOneTrainer;

    }

    @Override
    public TrainerEntity create(TrainerEntity trainerEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainerEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Trainer was created. Id" + trainerEntity.getId());
        return trainerEntity;
    }

    @Override
    public TrainerEntity update(TrainerEntity trainerEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(trainerEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Trainer was updated. Id" + trainerEntity.getId());
        return trainerEntity;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        TrainerEntity foundTrainer = em.find(TrainerEntity.class, id);
        em.remove(foundTrainer);

        em.getTransaction().commit();
        em.close();
        System.out.println("Trainer was deleted");

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }


}
