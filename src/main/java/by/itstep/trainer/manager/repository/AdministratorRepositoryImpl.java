package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.AdministratorEntity;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class AdministratorRepositoryImpl implements AdministratorRepository {
    @Override
    public List<AdministratorEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<AdministratorEntity> foundAllAdmin = em.createNativeQuery("SELECT * FROM administrator",
                AdministratorEntity.class).getResultList();

        em.close();
        System.out.println("found" + foundAllAdmin.size() + " admins");
        return  foundAllAdmin;
    }

    @Override
    public AdministratorEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        AdministratorEntity foundOneAdmin = em.find(AdministratorEntity.class, id);

        em.close();
        System.out.println("found admin: " + foundOneAdmin);
        return foundOneAdmin;

    }

    @Override
    public AdministratorEntity create(AdministratorEntity administratorEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(administratorEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Admin was created. Id" + administratorEntity.getId());
        return administratorEntity;
    }

    @Override
    public AdministratorEntity update(AdministratorEntity administratorEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(administratorEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Admin was updated. Id" + administratorEntity.getId());
        return administratorEntity;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        AdministratorEntity foundAdministrator = em.find(AdministratorEntity.class, id);
        em.remove(foundAdministrator);

        em.getTransaction().commit();
        em.close();
        System.out.println("Admin was deleted");

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM administrator").executeUpdate();

        em.getTransaction().commit();
        em.close();


    }


}
