package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.AdministratorEntity;

import java.util.List;

public interface AdministratorRepository {

    List<AdministratorEntity> findAll();

    AdministratorEntity findById(Long id);

    AdministratorEntity create (AdministratorEntity administratorEntity);

    AdministratorEntity update (AdministratorEntity administratorEntity);

    void deleteById(Long id);

    void deleteAll();
}
