package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.entity.CommentEntity;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class CommentRepositoryImpl implements CommentRepository {
    @Override
    public List<CommentEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CommentEntity> foundAllComment = em.createNativeQuery("SELECT * FROM comment",
                CommentEntity.class).getResultList();

        em.close();
        System.out.println("found" + foundAllComment.size() + " comments");
        return  foundAllComment;
    }

    @Override
    public CommentEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        CommentEntity foundOneComment = em.find(CommentEntity.class, id);

        em.close();
        System.out.println("found comment: " + foundOneComment);
        return foundOneComment;

    }

    @Override
    public CommentEntity create(CommentEntity commentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(commentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was created. Id" + commentEntity.getId());
        return commentEntity;
    }

    @Override
    public CommentEntity update(CommentEntity commentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(commentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was updated. Id" + commentEntity.getId());
        return commentEntity;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        CommentEntity foundComment = em.find(CommentEntity.class, id);
        em.remove(foundComment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Comment was deleted");

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }


}
