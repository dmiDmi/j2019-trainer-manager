package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.entity.AppointmentEntity;
import by.itstep.trainer.manager.util.EntityManagerUtils;

import javax.persistence.EntityManager;
import java.util.List;

public class AppointmentRepositoryImpl implements AppointmentRepository {
    @Override
    public List<AppointmentEntity> findAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();

        List<AppointmentEntity> foundAllAppointment = em.createNativeQuery("SELECT * FROM appointment",
                AppointmentEntity.class).getResultList();

        em.close();
        System.out.println("found" + foundAllAppointment.size() + " appointment");
        return  foundAllAppointment;
    }

    @Override
    public AppointmentEntity findById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();

        AppointmentEntity foundOneAppointment = em.find(AppointmentEntity.class, id);

        em.close();
        System.out.println("found appointment: " + foundOneAppointment);
        return foundOneAppointment;

    }

    @Override
    public AppointmentEntity create(AppointmentEntity appointmentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(appointmentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was created. Id" + appointmentEntity.getId());
        return appointmentEntity;
    }

    @Override
    public AppointmentEntity update(AppointmentEntity appointmentEntity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(appointmentEntity);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was updated. Id" + appointmentEntity.getId());
        return appointmentEntity;
    }

    @Override
    public void deleteById(Long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        AppointmentEntity foundAppointment = em.find(AppointmentEntity.class, id);
        em.remove(foundAppointment);

        em.getTransaction().commit();
        em.close();
        System.out.println("Appointment was deleted");

    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM appointment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }


}
