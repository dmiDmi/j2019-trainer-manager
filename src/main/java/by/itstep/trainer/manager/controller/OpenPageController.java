package by.itstep.trainer.manager.controller;

import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class OpenPageController {

    private TrainerService trainerService = new TrainerServiceImpl();
    private AppointmentService appointmentService = new AppointmentServiceImpl();
    private CommentService commentService = new CommentServiceImpl();

    //получить главную страницу
    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {

        List<TrainerPreviewDto> found = trainerService.findAll();

        model.addAttribute("all_trainer", found);

        return "index" ;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainers/open/{id}")
    public String openTrainerPage(@PathVariable Long id, Model model) {

        TrainerFullDto foundTrainer = trainerService.findById(id);
        model.addAttribute("trainer", foundTrainer);
        model.addAttribute("appointments", foundTrainer.getAppointments());
        model.addAttribute("comments", foundTrainer.getComments());
        model.addAttribute("commentIn", new CommentCreateDto());


         return "trainer";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/comments/create")
    public String saveComment(CommentCreateDto commentCreateDto) {

        try {
            commentService.create(commentCreateDto);
        } catch (InvalidDtoException e) {
            e.printStackTrace();
            return "redirect:/oops";
        }
        return "redirect:/index";
    }
}
