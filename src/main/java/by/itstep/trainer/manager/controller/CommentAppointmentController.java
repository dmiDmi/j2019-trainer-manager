package by.itstep.trainer.manager.controller;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentAppointmentController {


    private AppointmentService appointmentService = new AppointmentServiceImpl();
    private TrainerService trainerService = new TrainerServiceImpl();
    private CommentService commentService = new CommentServiceImpl();


    @RequestMapping(method = RequestMethod.GET, value = "/appointments/open/{trainerId}")
    public String openAppointmentPage(@PathVariable Long trainerId, Model model) {
        TrainerFullDto found = trainerService.findById(trainerId);
        model.addAttribute("trainerOne", found);
        model.addAttribute("createAppointment", new AppointmentCreateDto());

        return "appointment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/appointments/create")
    public String saveAppointment(AppointmentCreateDto createDto) {

        try {
            appointmentService.create(createDto);
        }catch (InvalidDtoException e) {
            e.printStackTrace();
            return "redirect:/oops";
        }
        return "redirect:/index"  ;
    }

//    @RequestMapping(method = RequestMethod.GET, value = "/comments/open/{id}")
//    public String openCommentPage(@PathVariable Long id, Model model) {
//
//        TrainerFullDto foundTrainer = trainerService.findById(id);
//        model.addAttribute("trainerComment", foundTrainer);
//        model.addAttribute("commentIn", new CommentCreateDto());
//
//        return "trainer";
//    }

//    @RequestMapping(method = RequestMethod.POST, value = "/comments/create")
//    public String saveComment(CommentCreateDto commentCreateDto) {
//
//        try {
//            commentService.create(commentCreateDto);
//        } catch (InvalidDtoException e) {
//            e.printStackTrace();
//            return "redirect:/oops";
//        }
//        return "redirect:/index";
//    }

    @RequestMapping(method = RequestMethod.GET, value = "/oops")
    public String showErrorPage() {
        return "funny-error" ;

    }
}
