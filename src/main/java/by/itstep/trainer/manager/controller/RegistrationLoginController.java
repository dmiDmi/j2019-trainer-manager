package by.itstep.trainer.manager.controller;

import by.itstep.trainer.manager.dto.administrator.AdministratorPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerLoginDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.exception.InvalidDtoException;
import by.itstep.trainer.manager.service.AdministratorService;
import by.itstep.trainer.manager.service.AdministratorServiceImpl;
import by.itstep.trainer.manager.service.TrainerService;
import by.itstep.trainer.manager.service.TrainerServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class RegistrationLoginController {

    private TrainerService trainerService = new TrainerServiceImpl();


//    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
//    public String openProfile(@PathVariable Long id, Model model) {
//
//        TrainerFullDto found = trainerService.findById(id);
//        model.addAttribute("trainerOne", found);
//        return "trainer";
//    }

    // нужен чтобы открыть форму!
    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openRegistrationPage(Model model) {
        model.addAttribute("createDto", new TrainerCreateDto());
    //    model.addAttribute("loginDto", new TrainerLoginDto());
        return "reg";
    }

    // нужен чтобы принять запрос на сохранение из формы!
    @RequestMapping(method = RequestMethod.POST, value = "/trainers/create" )
    public String saveTrainer(TrainerCreateDto createDto) {

        try {
            TrainerFullDto created = trainerService.create(createDto);
        }catch (InvalidDtoException e) {
            e.printStackTrace();
            return "redirect:/oops";
        }
        return "redirect:/registration";
    }

//    @RequestMapping(method = RequestMethod.POST, value = "/trainers/login")
//    public String login(TrainerLoginDto loginDto) {
//        return "redirect:/registration" ;
//    }


}
